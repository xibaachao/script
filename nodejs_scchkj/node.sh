wget https://cdn.0305.ink/debian/script/nodejs_scchkj/node-v16.16.0-linux-x64.tar.xz &&
###
 # @Author: 代码侠
 # @Date: 2022-04-11 10:22:30
 # @Email: achao@achao.cc
 # @LastEditTime: 2022-08-02 14:42:39
 # @Mobile: 18000599588
### 
tar -xf node-v16.16.0-linux-x64.tar.xz &&
mv node-v16.16.0-linux-x64 /usr/local/nodejs &&
rm -rf node-v16.16.0-linux-x64.tar.xz &&
ln -s /usr/local/nodejs/bin/node /usr/local/bin/ &&
ln -s /usr/local/nodejs/bin/npm /usr/local/bin/ &&
npm install -g yarn --registry=https://registry.npmmirror.com &&
npm config set registry https://registry.npmmirror.com &&
ln -s /usr/local/nodejs/lib/node_modules/yarn/bin/yarn /usr/local/bin/ &&
yarn config set registry https://registry.npmmirror.com &&
npm install -g git-cz &&
ln -s /usr/local/nodejs/bin/gitcz /usr/local/bin/ ;
ln -s /usr/local/nodejs/bin/git-cz /usr/local/bin/ ;