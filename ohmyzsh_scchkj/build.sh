###
 # @Author: 代码侠 
 # @Date: 2022-04-06 10:00:52
 # @Email: achao@achao.cc
 # @LastEditTime: 2022-08-02 14:17:15
 # @Mobile: 18000599588
### 
apt-get install -y zsh &&
curl -L https://gitee.com/mirrors/oh-my-zsh/raw/master/tools/install.sh | bash &&
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh}/plugins/zsh-syntax-highlighting &&
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh}/plugins/zsh-autosuggestions &&
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-~/.oh-my-zsh}/plugins/zsh-completions &&
sed -i '/^plugins=/c\plugins=(git z zsh-syntax-highlighting zsh-autosuggestions zsh-completions)' ~/.zshrc &&
[ -z "`grep "autoload -U compinit && compinit" ~/.zshrc`" ] && echo "autoload -U compinit && compinit" >> ~/.zshrc &&
source ~/.zshrc
chsh -s /bin/zsh