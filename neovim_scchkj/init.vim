set nu
set wrap
set tabstop=4
set expandtab
set smartindent
set shiftwidth=4
call plug#begin('~/.vim/plugged')
Plug 'crusoexia/vim-monokai'
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight' " enhance devicons
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
let g:molokai_original = 1
let g:rehash256 = 1
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()
colorscheme monokai
""NerdTree插件的配置信息
""打开vim时,自动打开NERDTree
autocmd vimenter * NERDTree
""将F2设置为开关NERDTree的快捷键
map <F2> :NERDTreeMirror<CR>
map <f2> :NERDTreeToggle<cr>
""修改树的显示图标
let g:NERDTreeDirArrowExpandable = '+'
let g:NERDTreeDirArrowCollapsible = '-'
""窗口位置
let g:NERDTreeWinPos='left'
""窗口尺寸
let g:NERDTreeSize=30
""窗口是否显示行号
let g:NERDTreeShowLineNumbers=1
""显示隐藏文件
let NERDTreeShowHidden=1
""设置左边的宽度
let NERDTreeWinSize=20 