###
 # @Author: 代码侠
 # @Date: 2022-04-12 10:06:28
 # @Email: achao@achao.cc
 # @LastEditTime: 2022-08-02 14:02:21
 # @Mobile: 18000599588
### 
wget https://cdn.0305.ink/debian/script/golang_scchkj/go1.19.linux-amd64.tar.gz &&
tar -C /usr/local -xzf go1.19.linux-amd64.tar.gz &&
rm -rf go1.19.linux-amd64.tar.gz &&
echo 'export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin'>> ~/.bash_profile &&
source ~/.bash_profile &&
go version &&
go env -w GO111MODULE=on &&
go env -w GOPROXY=https://goproxy.cn,direct