###
 # @Author: 代码侠
 # @Date: 2022-04-03 14:53:46
 # @Email: achao@achao.cc
 # @LastEditTime: 2022-08-15 16:58:22
 # @Mobile: 18000599588
### 
apt-get update -y 
apt-get install -y libcurl openssl
cd /home && 
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-debian11-6.0.0.tgz &&
tar -zxvf mongodb-linux-x86_64-debian11-6.0.0.tgz &&
rm -rf mongodb-linux-x86_64-debian11-6.0.0.tgz &&
mv mongodb-linux-x86_64-debian11-6.0.0  /usr/local/mongodb6 &&
mkdir -p /usr/local/mongodb6/conf &&
cd /usr/local/mongodb6/conf &&
wget https://cdn.0305.ink/debian/script/mongo_scchkj/mongodb.conf &&
mkdir -p /home/mongodb6/db/ &&
mkdir -p /home/mongodb6/log/ &&
touch /home/mongodb6/log/mongdb.log &&
ln -s /usr/local/mongodb6/bin/mongod /usr/local/bin/ &&
cd /etc/profile.d/ &&
touch start_mongodb.sh &&
echo mongod --config /usr/local/mongodb6/conf/mongodb.conf >> start_mongodb.sh
