###
 # @Author: 代码侠 
 # @Date: 2022-03-31 14:14:31
 # @Email: achao@achao.cc
 # @LastEditTime: 2022-08-02 14:02:38
 # @Mobile: 18000599588
### 
wget https://cdn.0305.ink/debian/script/neovim_scchkj/nvim-linux64.tar.gz &&
tar xf nvim-linux64.tar.gz &&
mv nvim-linux64 /usr/local/nvim &&
rm -rf nvim-linux64.tar.gz &&
ln -s /usr/local/nvim/bin/nvim /usr/local/bin/ &&
mkdir ~/.config &&
mkdir ~/.config/nvim &&
cd ~/.config/nvim &&
wget https://cdn.0305.ink/debian/script/neovim_scchkj/init.vim &&
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim' &&
npm install -g neovim