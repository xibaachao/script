<!--
 * @Author: 代码侠
 * @Date: 2022-03-31 10:33:56
 * @Email: achao@achao.cc
 * @LastEditTime: 2022-08-15 16:03:12
 * @Mobile: 18000599588
-->

### 环境搭建脚本(个人debian 安装脚本，仅限本人使用)

#### base 基础环境安装

```bash
wget https://gitea.com/xibaachao/script/raw/branch/master/base_script/base.sh && sh base.sh
```

#### docker

```
curl https://gitea.com/xibaachao/script/raw/branch/master/docker_scchkj/build.sh | bash
```

#### golang 1.9 环境安装，并配置国内源

  安装脚本

```
curl https://gitea.com/xibaachao/script/raw/branch/master/golang_scchkj/install.sh | bash
```

#### mongdb安装脚本

```
curl https://cdn.0305.ink/debian/script/mongo_scchkj/mongo.sh | bash
```

#### nodejs 16.0 安装脚本

- nodejs 环境安装，并配置了国内源
  安装脚本

```bash
curl https://gitea.com/xibaachao/script/raw/branch/master/nodejs_scchkj/node.sh | bash
```

#### python3.9 环境安装

```bash
curl https://gitea.com/xibaachao/script/raw/branch/master/python_scchkj/python.sh | bash
```



#### ohmy_zsh

```
curl -L https://cdn.0305.ink/debian/script/ohmyzsh_scchkj/build.sh | bash
```

#### jenkins

```
curl https://gitee.com/scchkj/script/raw/master/jenkins_scchkj/build.sh | bash

进入容器后查看密码
cat /var/jenkins_home/secrets/initialAdminPassword
```

#### samba

```
curl https://gitee.com/scchkj/script/raw/master/samba_scchkj/build.sh | bash
smbpasswd -a root
```

#### neovim

```
curl https://gitee.com/scchkj/script/raw/master/neovim_scchkj/neovim.sh | bash
```

#### php环境

```
curl https://gitea.com/xibaachao/script/raw/branch/master/php_scchkj/php.sh | bash

```

### postgreSql环境

```
curl https://cdn.0305.ink/debian/script/postgre_scchkj/install.sh | bash

```