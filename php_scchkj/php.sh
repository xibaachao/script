###
 # @Author: 代码侠 
 # @Date: 2022-07-29 15:39:11
 # @Email: achao@achao.cc
 # @LastEditTime: 2022-08-02 14:02:47
 # @Mobile: 18000599588
### 
apt-get install ca-certificates apt-transport-https software-properties-common -y &&
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list
wget -qO - https://packages.sury.org/php/apt.gpg |  apt-key add - &&
apt-get update -y &&
apt install php8.1-fpm -y &&
apt-get install php-curl -y &&
apt-get install php-dev -y &&
apt-get install php-gd -y  &&
apt-get install php-mbstring -y &&
apt-get install php-mysql -y  &&
apt-get install php-zip -y &&
apt-get install php-redis -y  &&
apt-get install php-pgsql -y &&
php -r "copy('https://install.phpcomposer.com/installer', 'composer-setup.php');" &&
php composer-setup.php &&
mv composer.phar /usr/local/bin/composer &&
php -r "unlink('composer-setup.php');" &&
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/ &&
wget http://pear.php.net/go-pear.phar &&
php go-pear.phar &&
rm -rf go-pear.phar &&
pecl install swoole &&
echo extension=swoole.so >> /etc/php/8.1/cli/php.ini